#include "dllapi.h"
#include "dllapi_p.h"
#include <string>
#include <iostream>

namespace dllapi {
namespace test {
DEFINE_DLL_INSTANCE("z")
DEFINE_DLLAPI_ARG(0, const char*, zlibVersion)
} //namespace test
} //namespace dllapi
